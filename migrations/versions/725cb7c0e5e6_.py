"""Add discord_id to user

Revision ID: 725cb7c0e5e6
Revises: c2e487a0fce9
Create Date: 2022-11-03 10:49:55.997384

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '725cb7c0e5e6'
down_revision = '0beab3563fe7'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("user", sa.Column("discord_id", sa.String(), nullable=True))


def downgrade():
    op.drop_column("user", "discord_id")
