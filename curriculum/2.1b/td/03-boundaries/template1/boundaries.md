# Boundaries


## ExampleContainerA <--> ExampleContainerB (EXAMPLE)

### Base protocol: RESTful HTTP (EXAMPLE)

(EXAMPLE)

A RESTful HTTP protocol is easy to understand and easy to use as a server in ExampleContainerA and as a client ExampleContainerB. 

Compared to binary options, like gRPC and protobuf, it is easier to use but slower. In this case BLABLABLA.

WebSocket is not ideal in this case because BLABLABLA.

Lower-level protocols like TCP and UDP are BLABLABLA.

Modern REST-competitors like GraphQL are worth considering in this case because BLABLABLA. However, BLABLABLA.

### Specification

(EXAMPLE)
See [OpenAPI spec](my-example-protocol.yaml).


## ExampleContainerB <--> ExternalSystemA (EXAMPLE)

### Protocol: SMTP (EXAMPLE)

(EXAMPLE) SMTP (including its encrypted variants) is the standard protocol for exchanging emails, and it is the only option ExternalSystemA supports for receiving email to relay. Therefore, the choice was easy.


## ExampleContainerA <--> DatabaseA (EXAMPLE)

### Protocol: MySQL Client/Server Protocol (EXAMPLE)

This is the obvious choice, as it is the only protocol offered by our MySQL database. It's also conveniently supported by BLABLABLA in our ExampleContainerA.

### Specification

(EXAMPLE)
```plantuml
entity Tree {
    * id INTEGER AUTO_INCREMENT
    ---
    * latitude FLOAT
    * longitude FLOAT
    * tree_type_id INTEGER
    forest_id INTEGER
}
entity TreeType {
    * id INTEGER AUTO_INCREMENT
    ---
    * name TEXT
}
entity Forest {
    * id INTEGER AUTO_INCREMENT
    ---
    * name TEXT
    avg_height FLOAT
}
Tree }o--|| TreeType
Forest |o--|{ Tree
```


# C4 level 2 model with protocols

TODO: Copy from previous assignment and add the (base) protocol names to the relations.
