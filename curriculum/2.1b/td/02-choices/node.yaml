name: Making choices
description: Choose not just the first solution that you can think of, but the best one. How to explore options and decide?
goals:
    decisions: 1
resources:
    Exploring alternatives:
    -
        link: https://alternativeto.net/software/flutter-dart/
        title: AlternativeTo - Flush Alternatives
        info: This website is one way to explore alternative technologies to use, if you know the name of one suitable technology. Keep it mind that it may not help you think outside-the-box.
    -
        link: https://www.elegantthemes.com/blog/wordpress/tools-to-see-what-software-a-website-was-built-with
        title: 4 Tools to See What Software a Website Was Built With (And Why You’d Want To)
        info: Another way to come up with alternatives is to try to learn what technology similar (successful!) apps are using. This page lists some tools that may help you do that.
    - |
        And of course you'll use your favorite search engine to come up with alternatives. Try to use various search terms, thinking outside-the-box. So instead of just searching for `flask alternative`, see what hits you get when you're more specific `python api framework` or `rapidly build python backend`. Also, don't forget that a cloud service (such as Google Firebase), as opposed to an (open source) product (such as MongoDB), *may* be the best alternative for a particular situation.

    Making and motivating technical decisions:
    -
        link: https://www.youtube.com/watch?v=hbMrnQiZkEw&t=65s
        title: 5 tips for how to make Technical Decisions as a Software Engineer
    -
        link: https://medium.com/swlh/decision-management-in-software-engineering-ca60f9d40e02
        title: Decision Management in Software Engineering
    -
        link: https://toggl.com/blog/decision-matrix
        title: Need to Make a Tough Decision? A Decision Matrix Can Help
    -
        link: https://www.decisionskills.com/uploads/5/1/6/0/5160560/worksheet_-_weighted_pros_and_cons.pdf
        title: Weighted pros and cons - Worksheet
        info: The *weighted pros and cons* technique can help you make a yes/no decision.

assignment:
- |
    We'll continue working on the Board Game Extension Manager case. You can start by copying your C2 model from the previous node.

    We want you to decide which technology to use for each of the containers and which service to use for each of the external systems.

    Here are some things you should know about the business:

    - We want to have Extension Manager apps for our customers in the iOS App Store and in the Android Play Store.
    - We have only a small development team, so we're willing take shortcuts. The team has experience with web development using PHP and Django. We also have some experience doing Javascript and React.
    - We want to keep the extension manager system separate from our regular website, which is mostly maintained by the marketing department using Wordpress.
    - We run most of our software on AWS (Amazon Web Services). Mostly EC2. Up til know (MongoDB and PostgreSQL) database maintenance has been done by our development team, but we're interested in exploring database-as-a-service options.

-
    title: Integrate backend with web app server?
    text: |
        You need to make a decision: will your architecture have a separate backend server and a separate web app server, or will these functionalities be part of the same application (serving both HTML and, let's say, a REST API)?

        Provide arguments in favour of both options, and motivate your final decision.
    4: Sensible list of pros/cons. Sensible and well-motivated decision.

-
    title: Container technology
    weight: 2
    text: |
        For each of the containers, choose on which technology they should be based. That could be something like a programming language and a framework, but could also be a service provided 'in the cloud' (in which case you should name a specific service from a specific company).

        For each decision...
        - Explore alternative.
        - Compare the most viable three/four alternative, writing down their pros/cons.
        - Choose one, and explain why.

        The template already contains a partial example you can extend. But feel free to pick another format and/or change any items. *Hint:* install the *Markdown Table* Visual Studio Code extension by *TakumiI*, in order to edit Markdown tables without losing your sanity.

    4: Most important alternatives identified. Sensible list of pros/cons. Sensible and well-motivated decision.

-
    title: External systems
    text: |
        For each of the external systems, choose a specific company (and a specific service, in case the company is offering more than one).

        As before, motivate your decision by comparing three/four alternatives.
    4: Most important alternatives identified. Sensible list of pros/cons. Sensible and well-motivated decision.

-
    title: Update your C2
    text: |
        Copy your C2 level model from the previous assignment, and add your chosen technologies to the containers and external services.
    must: true