# Use case overview

```plantuml
left to right direction
actor "User" as user
actor "Admin" as admin
rectangle App {
  usecase "Register User" as UC1
  usecase "Log In" as UC4
  usecase "Add to Basket" as UC2
  usecase "Check Inventory" as UC2a
  usecase "Checkout" as UC3
  usecase "Update Inventory" as UC5
  usecase "See Inventory" as UC6
  usecase "Send Invoice" as UC7
}
user -- UC4
user -- UC2
(UC2) .> (UC2a) : <<include>>
user -- UC3
UC1 -- admin
UC5 -- admin
UC6 -- admin
UC7 -- admin
```