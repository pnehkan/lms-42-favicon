import postgresqlite, datetime

# Start (and initialize) the database and connect to it.
db = postgresqlite.connect()

# Insert a row.
test_id = db.query_value("INSERT INTO test(gibberish) VALUES(:gib) RETURNING id", gib="Nonsense!")
print("Inserted with id", test_id)

# Iterate all result rows for a query.
for row in db.query("SELECT * FROM test"):
    print(f"{row['id']}. {row['gibberish']} at {row['created_at'].time()}")

# You may also do it like this, if you prefer:
for row in db.query("SELECT * FROM test"):
    print(f"{row.id}. {row.gibberish} at {row.created_at.time()}")

# A query returning a single columns as a list of values.
last_month = datetime.date.today() - datetime.timedelta(days=30)
for gibberish in db.query_column("SELECT DISTINCT gibberish FROM test WHERE DATE(created_at) > :date", date=last_month):
    print(f"- {gibberish}")
