- |
    In this assignment we will play around with _inheritance_, a key concept in object-oriented programming.
    
    Let's build a collection of shapes and colors to get a good understanding of the subject.

    <div class="notification is-info">
        The end result of what your application should output is shown at the bottom of this assignment. You might want to take a peek in case you get confused about what you're meant to achieve.
    </div>

-
    title: Shape
    ^merge: generic
    text: |
        Create a class named `Shape`. Every shape should be initialized with a size, an x position and a y position (pointing at the center of the shape). So one should be able to create an shape with size 100 at position x=200 y=0 like this: `Shape(100, 200, 0)`.

        When printing a shape (eg `print(my_shape)`) it should display something like: "shape of size 100 positioned at (200, 0)".

        In addition, it should have two *getter* methods:

        - `get_size()`, returning the shape's size, and
        - `get_position()`, returning a [tuple](https://www.programiz.com/python-programming/tuple) with the x and y position.

- 
    title: Square and Circle
    ^merge: generic
    text: |
        Shapes come in many forms and sizes. Create two subclasses of `Shape`: `Square` and `Circle`.

        Override their `__str()__` methods such that..
        
        - a `print(Square(100,0,0))` prints as "square of size 100 positioned at (0, 0)", and
        - a `print(Circle(100,0,0))` prints as "circle with radius 50 positioned at (0, 0)". (We interpret the size here as the circle's diameter. The radius (Dutch: *straal*) of a circle is half its diameter.)

        Also implement `get_area` methods for both classes. They should calculate and return the [area](https://www.omnicalculator.com/math/area-of-a-circle) of the shape, based on its size. 

-
    title: Create shapes
    ^merge: generic
    text: |
        Within the `create_shapes` function...

        - Use a loop to create a *list* of 50 shapes, where each shape is (randomly) either a square or a circle, and has a random size between 1 and 100, a random x position between 0 and 800 and a random y position between 0 and 800.

- Polymorphism:
    - 
        link: https://www.geeksforgeeks.org/polymorphism-in-python/
        title: Polymorphism in Python
        info: The word polymorphism means having many forms. In programming, polymorphism means same function name (but different signatures) being uses for different types
    - 
        link: https://www.youtube.com/watch?v=CuK0g8OFzwo&list=PLsyeobzWxl7poL9JTVyndKe62ieoN-MZ3&index=62&ab_channel=Telusko
        title: Python Tutorial for Beginners | Duck Typing
        info: One of the ways polymorphism is used in Python is through the concept of duck typing. This video explains what that is.

-
    title: Print shapes
    ^merge: generic
    text: |
        Within the `print_shapes` function...

        - Iterate over the provided list of shapes, printing something like this for each shape: "I'm a circle with radius 35 position at (592, 73). My area is 3848." Note that this does *not* require an `if` statement. You should use polymorphism to have Python choose the appropriate `__str__`.
        - Print the total combined area of all shapes. Again, no `if` should be required.

-
    title: Create HTML
    ^merge: generic
    text: |
        For both `Square` and `Circle`, implement `get_css()` methods that return CSS strings that can be applied to a `<div>` element to have is displayed as the shape. You can use `position: absolute;` for position. You can pick a background color you like. Use 50% opacity to create semi-transparent shapes, so that you can see the shapes that would otherwise be hidden behind other shapes.
        
        For example:
        
        ```
        background-color: #db0651;
        width: 55px;
        height: 96px;
        position: absolute;
        left: 132px;
        bottom: 730px;
        border-radius: 50%;
        opacity: 0.5;
        ```

        Complete the `write_shapes_html` function to generate your work of art. The generated `shapes.html` should look something like this when opened in a browser:

        ![Intermediate work of art](shapes1.png)

        
- Super:
    -
        link: https://www.digitalocean.com/community/tutorials/python-super
        title: Python super() - Python 3 super()
        text: |
            How to use `super()` to call base class (also known as *superclass*) methods that have been overriden by a subclass.

-
    title: Rectangle
    ^merge: generic
    text: |
        Create a `Rectangle` class, subclassing `Shape`.
        
        - One should be able to instantiate a rectangle like this `Rectangle(x, y, width, height)`. This will require `Rectangle` to have its own constructor method.
        - `Rectangle`'s constructor should use `super()` to call the base class' constructor to initialize `Shape`s attributes. We'll (rather arbitrarily) define the `size` of a rectangle to be equal to its width.
        - Implement `get_width` and `get_height` methods. It is good practice never to store duplicate information, or information that can be derived from other information, in your class attributes. Therefore, `Rectangle` should not store `width` as an attribute, as that would duplicate the `size` of the `Shape`. (What would it mean if someone were to change `size` without changing `width`?)
        - Add `Rectangle` to your `create_shapes` function. Make sure `print_shapes` and `draw_shapes` now work for `Rectangle`s as well (without having to change these functions).

-
    title: Squares are rectangles too
    ^merge: generic
    text: |
        A square is just a special case of a rectangle. Change your `Square` implementation such that it's based upon `Rectangle`, reducing the amount of code as much as possible. You should not have to change anything in your `create_shapes` function.

-
    title: Pyramids
    ^merge: generic
    text: |
        Add another shape: the `Pyramid`.

        *Hints*:

        - The area of a pyramid is half the area of the square one could fit around it.
        - There's a [simple trick](https://www.freecodecamp.org/news/css-shapes-explained-how-to-draw-a-circle-triangle-and-more-using-pure-css/#triangles) to draw triangles in CSS.

-
    title: Rounded rectangles
    ^merge: generic
    text: |
        Add a new `RoundedRectangle` shape. It should work the same as a normal `Rectangle`, except that its constructor expects an extra *border_radius* argument.

        Add your new class to `create_shapes`, and make sure that it works with `print_shapes` and `draw_shapes`.

        Hints:
        
        - The maximum border radius should be half the width or half the height of the shape, whichever is smaller.
        - The area of a rounded rectangle can by calculated like this: width\*height - border_radius[²](https://favtutor.com/blogs/square-number-python) \* (4 - [π](https://www.w3schools.com/python/ref_math_pi.asp))

-
    title: Almost everything is a rounded rectangle
    ^merge: generic
    text: |
        Note that all of our existing shapes, except one, are special cases of a rounded rectangle. Use this fact to reduce the amount of code as much as possible.

        After this, you should only have two `get_css()` and two `get_area()` implementations left.

-
    title: Different colors
    ^merge: generic
    text: |
        Finally, we want each of the different types of shapes to have its own colors. For instance, all circles could be red while all squares are blue.

        We want to achieve this *without* each shape class having to implement its own `get_css()` method again.

        *Hint*: a base class (or superclass) can use polymorphism to call a method defined in a subclass.

- Expected output:
    - |
        Your console output should look something like this:

        ```
        I'm a square of size 30 positioned at (647, 383). My area is 900.
        I'm a square of size 121 positioned at (93, 414). My area is 14641.
        I'm a Pyramid with size 29 positioned at (11, 41). My area is 420.
        I'm a Pyramid with size 123 positioned at (265, 18). My area is 7564.
        I'm a Pyramid with size 91 positioned at (283, 242). My area is 4140.
        I'm a circle with radius 72 positioned at (38, 215). My area is 2936.
        I'm a square of size 172 positioned at (374, 172). My area is 29584.
        I'm a circle with radius 29 positioned at (205, 81). My area is 476.
        I'm a rectangle of width 90 and height 9 positioned at (236, 37). My area is 810.
        I'm a square of size 104 positioned at (767, 209). My area is 10816.
        I'm a circle with radius 93 positioned at (84, 431). My area is 4899.
        I'm a square of size 48 positioned at (82, 403). My area is 2304.
        I'm a rectangle of width 105 and height 100 positioned at (516, 302). My area is 10500.
        I'm a circle with radius 8 positioned at (395, 519). My area is 36.
        I'm a rectangle of width 5 and height 62 positioned at (584, 318). My area is 310.
        I'm a rectangle of width 128 and height 32 positioned at (244, 163). My area is 4096.
        I'm a square of size 107 positioned at (199, 235). My area is 11449.
        I'm a Pyramid with size 76 positioned at (540, 126). My area is 2888.
        I'm a rounded rectangle of width 133 and height 16 with border radius 6 positioned at (512, 358). My area is 2097.
        I'm a rectangle of width 1 and height 34 positioned at (602, 535). My area is 34.
        I'm a rectangle of width 148 and height 78 positioned at (226, 562). My area is 11544.
        I'm a circle with radius 58 positioned at (231, 333). My area is 1938.
        I'm a rectangle of width 176 and height 57 positioned at (630, 104). My area is 10032.
        I'm a square of size 3 positioned at (319, 337). My area is 9.
        I'm a rounded rectangle of width 195 and height 98 with border radius 11 positioned at (84, 201). My area is 19006.
        I'm a rectangle of width 35 and height 15 positioned at (173, 137). My area is 525.
        I'm a rounded rectangle of width 17 and height 26 with border radius 16 positioned at (55, 131). My area is 222.
        I'm a Pyramid with size 125 positioned at (180, 124). My area is 7812.
        I'm a square of size 152 positioned at (617, 446). My area is 23104.
        I'm a rectangle of width 154 and height 6 positioned at (601, 285). My area is 924.
        I'm a rectangle of width 186 and height 14 positioned at (665, 408). My area is 2604.
        I'm a rounded rectangle of width 144 and height 27 with border radius 21 positioned at (510, 241). My area is 3509.
        I'm a rounded rectangle of width 142 and height 34 with border radius 3 positioned at (632, 249). My area is 4820.
        I'm a Pyramid with size 109 positioned at (617, 281). My area is 5940.
        I'm a rectangle of width 124 and height 29 positioned at (520, 75). My area is 3596.
        I'm a rectangle of width 102 and height 57 positioned at (335, 554). My area is 5814.
        I'm a rectangle of width 2 and height 58 positioned at (571, 208). My area is 116.
        I'm a rectangle of width 81 and height 46 positioned at (305, 82). My area is 3726.
        I'm a Pyramid with size 149 positioned at (440, 357). My area is 11100.
        I'm a rounded rectangle of width 54 and height 43 with border radius 19 positioned at (209, 267). My area is 2012.
        I'm a rounded rectangle of width 176 and height 23 with border radius 15 positioned at (482, 315). My area is 3855.
        I'm a circle with radius 65 positioned at (60, 302). My area is 2430.
        I'm a rounded rectangle of width 150 and height 32 with border radius 29 positioned at (291, 332). My area is 4078.
        I'm a square of size 34 positioned at (737, 393). My area is 1156.
        I'm a rectangle of width 152 and height 50 positioned at (569, 512). My area is 7600.
        I'm a rounded rectangle of width 79 and height 57 with border radius 26 positioned at (365, 168). My area is 3923.
        I'm a Pyramid with size 187 positioned at (449, 280). My area is 17484.
        I'm a circle with radius 41 positioned at (126, 597). My area is 975.
        I'm a circle with radius 31 positioned at (684, 212). My area is 544.
        I'm a rounded rectangle of width 181 and height 18 with border radius 1 positioned at (38, 314). My area is 3257.

        Total area: 274555
        ```

        The generated `shapes.html` should look something like this:

        ![Final work of art](shapes2.png)


- Code quality:
    -
        ^merge: codequality
        weight: 4
        text: |
            OOP code quality guidelines:
            - Only use private attributes, with double underscores.
            - Methods that are only to be used internally by the class itself, should also be   considered *private*.
            - Interact with instances of other classes not by reading/writing their attributes, but by calling a method, nicely asking the class to do something for you.
            - Put functionality in a method of the class that it (mostly) concerns. If it concerns multiple classes, see if you can split up the functionality.
            - Try to prevent creating (too many) setters and getters, as they imply the actual functionality acting on this class is being implemented outside of the class.
