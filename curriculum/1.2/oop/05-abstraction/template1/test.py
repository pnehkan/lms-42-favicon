from lightlib import *

# Create a room
kitchen = Room("kitchen")
assert kitchen.get_name() == "kitchen"
assert str(kitchen) == "kitchen {\n}"
assert Room.get("kitchen") is kitchen
assert Room.get("no-such-room") == None

# Create a light
kitchen_ceiling = WhiteLight("ceiling")
assert kitchen_ceiling.get_name() == "ceiling"
assert str(kitchen_ceiling) == "#1: ceiling → l0"

# Add a light to a room
kitchen.add_light(kitchen_ceiling)
assert kitchen.get_light("ceiling") is kitchen_ceiling
assert kitchen.get_light("no-such-light") == None
assert str(kitchen) == """kitchen {
  #1: ceiling → l0
}"""

# Set the light to maximum brightness
kitchen_ceiling.set_state(LightState(42))
assert str(kitchen) == """kitchen {
  #1: ceiling → l42
}"""

# The bedroom has three lights, including a color light
Room("bedroom").add_light(WhiteLight("bedside left"))
Room.get("bedroom").add_light(WhiteLight("bedside right"))
Room.get("bedroom").add_light(ColorLight("closet"))
assert str(Room.get("bedroom")) == """bedroom {
  #2: bedside left → l0
  #3: bedside right → l0
  #4: closet → l0 h0 s0
}"""

# Set romantic bedroom atmosphere
bedroom = Room.get("bedroom")
deep_red = ColorLightState(30, 324, 100)
bedroom.set_state(deep_red)
assert str(bedroom) == """bedroom {
  #2: bedside left → l30
  #3: bedside right → l30
  #4: closet → l30 h324 s100
}"""

# Some extra reading light on one side of the bed
bedroom.get_light("bedside right").set_state(LightState(50))
assert str(bedroom) == """bedroom {
  #2: bedside left → l30
  #3: bedside right → l50
  #4: closet → l30 h324 s100
}"""

# Save this as a scene
bedroom.store_scene('romantic reading')

# Set to full brightness, and save as another scene
bedroom.set_state(LightState(100))
assert str(bedroom) == """bedroom {
  #2: bedside left → l100
  #3: bedside right → l100
  #4: closet → l100 h0 s0
}"""
bedroom.store_scene('bright')

# Restore a scene
bedroom.recall_scene('romantic reading')
assert str(bedroom) == """bedroom {
  #2: bedside left → l30
  #3: bedside right → l50
  #4: closet → l30 h324 s100
}"""
bedroom.recall_scene('bright')
assert str(bedroom) == """bedroom {
  #2: bedside left → l100
  #3: bedside right → l100
  #4: closet → l100 h0 s0
}"""
