Sappy bird:
- |
    You've been handed a fully working program for a change. And it's a game. Hurray!
    
    You should be able to install and run it like this:

    ```sh
    poetry install && poetry run python sappy_bird.py
    ```

    The bird can be controlled with the *up*, *left* and *right* arrow keys.

Study the code:
-
    link: https://www.geeksforgeeks.org/getter-and-setter-in-python/
    title: Getter and Setter in Python
    info: |
        The code base you'll be studying makes extensive use of a Python feature you haven't seen before: `@property` and `@setter` decorators. This document provides an introduction.
-
    link: https://www.youtube.com/watch?v=6KidYEtspNc&t=1
    title: "Type Hinting - Advanced Python Tutorial #6"
    info: |
        The code base also uses *type hinting*. This video explains what it is, and how it can be used (through *mypy*) to find bugs in your code before even running it! Type hints are also picked up by automated documentation tools, which is why it's relevant in this lesson.   
-
    link: https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
    title: Type hints cheat sheet
    info: Some (advanced) examples of type hints. We recommend you just glance through the first half of this page to get an impression.

- |
    The game is build using the (pretty awesome) *pyglet* library, which is installed using poetry. To make it easier to create well-structured object-oriented games with *pyglet*, we have wrapped it with a small library of your own, provided as `gamelib.py`. Although it could use some more features, it should be able to serve as the basis for many 2D games, not just Flappy Bird clones.

    The game itself is implemented in `sappy_bird.py`. It doesn't use *pyglet* directly, but only through *gamelib*. Here's a high-level (lacking a lot of detail) class diagram of the whole thing:

    ```plantuml
    package gamelib {
        abstract class Game {
            {abstract} +start()
            {static} +run()
        }

        class Entity {
            +update()
        }
        Game <--> "0..*" Entity
        Game *---> pyglet.Window

        class SpriteEntity {}
        SpriteEntity --|> Entity
        SpriteEntity *---> pyglet.Sprite

        class LabelEntity {}
        LabelEntity --|> Entity
        LabelEntity *---> pyglet.Label
    }
    package sappy_bird {
        class SappyGame {
            +start()
        }
        SappyGame --|> Game

        Bird --|> SpriteEntity
        Background --|> SpriteEntity
        Pipe --|> SpriteEntity
        GameOver --|> SpriteEntity
        Score --|> LabelEntity
    }
    ```

    Study the code in `sappy_bird.py` and then in `gamelib.py`, and try to understand how it works. This is made a bit more challenging by the fact that *gamelib* is currently only partially documented. (*Hmm, now why could that possibly be? :-)*) 

    You may notice that we're breaking our *only use private attributes* rule quite a bit in this code. Base class instance attributes (such as `x` and `y` for the position) are being read and modified from subclasses. That's because code like `self.x += 1` is a lot more readable than `self.set_x(self.get_x() + 1)`. The exception proves the rule here. Also, single-underscore attributes are being used as a convention to indicate that an attribute may be accessed by other classes *in the same file*. Notice that the code still has a clear sense of which class 'owns' an attribute though.

-
    link: https://pyglet.readthedocs.io/en/latest/
    title: pyglet Documentation
    info: This is the official documentation for *pyglet*. You may need to refer to it for some details. In *API Reference* section (scroll the index on the left down) is good to have nearby.

Sappy Bird 2000 Max Ultimate edition:
- |
    
-
    ^merge: feature
    code: 0
    map: libraries
    text: |
        Let's add a little feature to make the game even better! This is mostly for you to test if you've understood the library - your actual job for this assignment is in the next objective.

        On *game over*, we want the text *Press space to play again* to show in the bottom center of the screen. To make it stand out more, we want it to quickly fade-in and fade-out. And when the user presses *space* we of course want the game to actually restart (for which you can use the `SappyGame.start()` method). You should be able to do all of this by creating a single new class in `sappy_bird.py`, and creating an instance of this class when the user dies.
# -
#     ^merge: feature
#     code: 0
#     map: libraries
#     text: |
#         To make the game harder the longer a player is playing, we want to introduce a *red zone* that gradually grows from the left of the screen. When the bird touches the red zone, it's game over. The *red zone* should be drawn as a partially transparent rectangle. Unfortunately, our *gamelib* doesn't have support for drawing rectangles yet. So your first step is to implement a new type of `Entity` in `gamelib.py`, based on [pyglet.shapes.Rectangle](https://pyglet.readthedocs.io/en/latest/modules/shapes.html#pyglet.shapes.Rectangle). Next you can use it to implement the *red zone* in `sappy_bird.py`.

#         [Timebox](https://www.agilealliance.org/glossary/timebox/): you may invest up to 1,5 hours working on this feature. No success? No worries. Just leave it be for now.

Documentation:
- 
    link: https://www.youtube.com/watch?v=URBSvqib0xw
    title: Learn Python Programming - PyDoc - A Celebration of Documentation
    info: Learn about docstrings, the built-in pydoc module, and how you use these to provide inline documentation for the classes and functions you write.

-
    link: https://pdoc3.github.io/pdoc/
    title: pdoc - Auto-generate API documentation for Python projects
    info: |
        Although the built-in *pydoc* module is nice, we'll be using something even better: *pdoc3*. This page explains what it is, and why it's better.

-
    link: https://www.youtube.com/watch?v=R6zeikbTgVc
    title: "Writing effective documentation | Beth Aitman | #LeadDevBerlin"
    info: Six quick tips on how to write technical documentation.

-
    link: https://www.youtube.com/watch?v=2xa9_A8HH3U
    title: Auto DocString Extension || Visual Studio Code
    info: This video (without sound) shows you how to install and use the *autoDocString* extension for VSCode. It'll save you time, and causes your documentation formatting to be consistent!

-
    map: inline-doc
    text: |
        Our `gamelib.py` library has been partially documented. Use *pdoc3* to build the HTML documentation:

        ```sh
        poetry add pdoc3
        poetry run pdoc3 gamelib.py --html --force
        ```
        
        Please complete the documentation in a style consistent with what's already there. All *public* methods and attributes (those that are meant to be accessed by games) should be documented such that a developer new to `gamelib.py` should be able to implement a game without looking at `gamelib.py` or any examples. Don't forget to explain the meaning of any method arguments and return values.
    0: The docs are useless / absent.
    2: The docs are helpful, but one would frequently need to refer to the code to study usage details.
    3: Good enough to make a quick start, without looking at the code.
    4: Documentation as one would expect for a well-known library.
