-
  short: Functional design
  description: Create a functional design (of limited complexity), based on the wishes of an (internal) customer.
  outcomes:
    "Write SMART business requirements.": [requirements,interview]
    "Write use cases based on requirements.": [usecases]
    "Draw wireframes for clarifying user stories.": [wireframing]
    "Apply the basics of graphics design.": [design]

-
  short: Technical design
  description: Create a technical design (of limited complexity), based on the wishes of an (internal) customer.
  outcomes:
    "Create high-level overviews of software architectures.": [architecture]
    "Describe real world objects and relations in a model.": [datamodeling,inheritance,class-diagram]
    "Model the behavior of non-trivial systems.": [state-diagram,seq-diagram]
    "Create graphical models of software designs.": [plantuml,class-diagram,state-diagram,seq-diagram]
    "Design APIs and interfaces.": [interfaces,rest]
    "Make methodical decisions.": [decisions]
    "Recognize and apply common design patterns.": [design-patterns]
    "Apply decomposition.": [decomposition]
    "Apply abstraction.": [abstraction]

-
  short: Implementation
  description: "Implement new software or extend existing software, based on a technical design, with regard for quality aspects: reliability, readability, adaptability, maintainability, security and performance."
  outcomes:
    "Program imperatively.": [vars,termio,flow,loops,comments,functions,strings,lists,dicts,fileio,naming]
    "Program object-oriented.": [modules,classes,static,exceptions,rust-polymorphism]
    "Program in a dynamically typed language.": [vars]
    "Program in a strict statically typed language.": [rust-basics,rust-generics]
    "Use concurrency.": [async,multithreading]
    "Debug programs.": [debugging]
    "Write reliable, readable, adaptable and maintainable code.": [refactor,design-patterns]
    "Select and use appropriate data structures.": [datastructures]
    "Extend pre-existing software.": [legacy_analyze,legacy_impl,legacy_contrib]
    "Document source code.": [inline-doc] 
    "Recognize and mitigate common security problems.": [web-security,crypto]

-
  short: Problem solving
  description: Solve problems using a solid theoretical basis.
  outcomes:
    "Implement and use algorithms and data structures.": [alg_impl,graphs,sorted_lists]
    "Select algorithms and datastructures based on their algorithmic complexities.": [alg_sel]
    "Understand the low-level workings of a computer.": [computerarchitecture,memstack,bindata]
    "Program using manual memory management.": [memheap,pointers]
    "Optimize software.": [rust-basics,performance,graphics]
    "Apply recursion and backtracking.": [recursion,backtracking]
    "Create translators for domain specific languages.": [regex,parsers,ast]

-
  short: Technology skills
  description: Hit the ground running with various commonly used programming techniques.
  outcomes:
    "Program in Javascript.": [javascript,nodejs]
    "Program in Java.": [java]
    "Implement Android applications.": [android-layouts,android-basics,android-navigation,android-lists,android-database,android-network]
    "Design and use SQL databases.": [queries,joins,aggregate,subqueries,schemas,indexes,foreignkeys,android-database,orm,python-sql,views]
    "Create web sites using modern HTML and CSS.": [html,css,responsive,htmlforms,icons-fonts]
    "Create dynamic server rendered web sites.": [flask,auth,web-security,web-forms]
    "Design and implement APIs.": [rest]
    "Create SPAs using a declarative UI framework.": [dom,svelte,spa,pwa]
    "Implement networked applications.": [ip,tcpudp,protocols,bindata,crypto]

-
  short: Self-directed learning
  description: Quickly and independently learn to use new programming techniques.
  outcomes:
    "Set and realize learning goals using a structured method.": During the three internships and three projects students are required to maintain a logbook in which they set their own learning goals and reflect on learning progress.
    "Quickly and independently learn to use a new programming language.": [javascript,java,rust-basics]
    "Quickly and independently learn to use a new framework.": [android-basics,svelte,flask]
    "Quickly and independently learn to use a new library.": [libraries,orm,nodejs,parsers]
    "Quickly and independently learn to use a new algorithm or pattern.": [alg_impl,backtracking]


-
  short: DevOps
  description: Automate software testing and deployment.
  outcomes:
    "Make effective use of unit testing.": [unittests]
    "Make effective use of end-to-end testing.": [e2etests]
    "Manage Linux systems.": [linux,scripting]
    "Use Docker.": [docker]
    "Use cloud platforms.": [cloud,infracode,cicd]
    "Setup a simple CI/CD environment.": [cicd]

-
  short: Teamwork
  description: Effectively collaborate as part of a development team, using modern tools and methodologies.
  outcomes:
    "Work effectively with the methodologies of modern software teams.": [projectmanagement]
    "Work effectively with modern collaboration tools.": [git,gitlab]
    "Understand the role of a software developer within a company.": [roles]
    "Present one's work.": [presentation]
    "Work mostly independently and set priorities.": [employee,personalmanagement]
    "Work as part of a small team.": [teamwork]
    "Listen comprehensively and argue clearly in technical discussions.": Students get daily training in technical communication through the mentoring system.
